const StorageUtils = {

	getToken(){
		let TokenObject = this.getTokenObject()
		return TokenObject && TokenObject.token;
	},

	getTokenObject(){
		let storedToken = localStorage.getItem('PortalToken') 
		if(storedToken == null){
			return null;
		}
		let TokenObject = JSON.parse(storedToken)
		let createTime = TokenObject.createTime
		let currentTime = new Date().getTime()
		// expired
		if(createTime + (TokenObject.tokenPeriod * 1000) < currentTime){
			localStorage.removeItem('PortalToken')
			return null;
		}
		return TokenObject;
	},

	getTokenAccount(){
		let TokenObject = this.getTokenObject()
		return TokenObject && TokenObject.account
	},

	removeTokenObject(){
		localStorage.removeItem('PortalToken')
	},

	setToken(tokenObject){
		let createTime = new Date().getTime()
		tokenObject.createTime = createTime
		localStorage.setItem('PortalToken' , JSON.stringify(tokenObject))
	},

	removeCookie(){

	},

	getCookie(cname){
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) 
		{
			var c = ca[i].trim();
			if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		}
		return "";
	}

	
}

export default StorageUtils;
